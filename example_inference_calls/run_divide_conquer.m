% Branching with smc inference, compressed output

clear all
close all

% Do inference on the branching model using smc
samples = ...
    infer('divide_conquer_simple_example',[],'smc','n_particles',5e5,'n_iter',3,'b_parallel',true,'b_compress',true);

summary = results_summary(samples,false);
histogram_plotter(samples,'x',200,1,1,false);
histogram_plotter(samples,'k',[],1,1,true);