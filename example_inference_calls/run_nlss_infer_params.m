% Nonlinear state space model with ipmcmc inference from the paper

dataset_to_use = 1;  % Scalar between 1 and 10

% Load data
data_nlss = load(['example_models' filesep() 'non_linear_state_space_data' filesep() 'nonlinear_state_space_generate_data' num2str(dataset_to_use) '.mat']);

% Choose number of steps
n_steps = 1000;
model_info.observations = data_nlss.Y(1:n_steps)';

n_particles = 1000000;
model_info.Vn_sigma = [0,10];
samples_nlss_big = infer('nonlinear_state_space_infer_params',model_info,'smc','n_particles',n_particles,'n_iter',1,'b_parallel',true,'b_compress',true);
histogram_plotter(samples_nlss_big,'Vn_sigma',300,1,1,false,1);

n_iter = 200;
n_particles = 2000;

model_info.Vn_sigma = [0,10];

% Run inference
samples_nlss = infer('nonlinear_state_space_infer_params',model_info,'smc','n_particles',n_particles,'n_iter',n_iter,'b_parallel',true,'b_compress',true);
log_Zs = samples_nlss.other_outputs.log_Zs;
[nall, xoutall] = hist(log_Zs,20);

model_info.Vn_sigma = [0,5];

% Run inference
samples_nlss_1 = infer('nonlinear_state_space_infer_params',model_info,'smc','n_particles',n_particles,'n_iter',n_iter,'b_parallel',true,'b_compress',true);
log_Zs_1 = samples_nlss_1.other_outputs.log_Zs;

model_info.Vn_sigma = [5,10];

samples_nlss_2 = infer('nonlinear_state_space_infer_params',model_info,'smc','n_particles',n_particles,'n_iter',n_iter,'b_parallel',true,'b_compress',true);
log_Zs_2 = samples_nlss_2.other_outputs.log_Zs;

figure
bar(xoutall,nall,'b'); hold on;
[n1, xout1] = hist(log_Zs_1,20);
bar(xout1,n1,'r'); grid;
[n2, xout2] = hist(log_Zs_2,20);
bar(xout2,n2,'g');
xlim = get(gca,'xlim');
ylim = get(gca,'ylim');
plot([-576.7223,-576.7223],ylim,'--k','LineWidth',8)

model_info.Vn_sigma = [0,2.5];

samples_nlss_1 = infer('nonlinear_state_space_infer_params',model_info,'smc','n_particles',n_particles,'n_iter',n_iter,'b_parallel',true,'b_compress',true);
log_Zs_1 = samples_nlss_1.other_outputs.log_Zs;

model_info.Vn_sigma = [2.5,5];

samples_nlss_2 = infer('nonlinear_state_space_infer_params',model_info,'smc','n_particles',n_particles,'n_iter',n_iter,'b_parallel',true,'b_compress',true);
log_Zs_2 = samples_nlss_2.other_outputs.log_Zs;

figure
bar(xoutall,nall,'b'); hold on;
[n1, xout1] = hist(log_Zs_1,20);
bar(xout1,n1,'r'); grid;
[n2, xout2] = hist(log_Zs_2,20);
bar(xout2,n2,'g');
set(gca,'xlim',xlim);
ylim = get(gca,'ylim');
plot([-576.7223,-576.7223],ylim,'--k','LineWidth',8)

model_info.Vn_sigma = [2.5,3.75];

samples_nlss_1 = infer('nonlinear_state_space_infer_params',model_info,'smc','n_particles',n_particles,'n_iter',n_iter,'b_parallel',true,'b_compress',true);
log_Zs_1 = samples_nlss_1.other_outputs.log_Zs;

model_info.Vn_sigma = [3.75,5];

samples_nlss_2 = infer('nonlinear_state_space_infer_params',model_info,'smc','n_particles',n_particles,'n_iter',n_iter,'b_parallel',true,'b_compress',true);
log_Zs_2 = samples_nlss_2.other_outputs.log_Zs;

figure
bar(xoutall,nall,'b'); hold on;
[n1, xout1] = hist(log_Zs_1,20);
bar(xout1,n1,'r'); grid;
[n2, xout2] = hist(log_Zs_2,20);
bar(xout2,n2,'g');
set(gca,'xlim',xlim);
ylim = get(gca,'ylim');
plot([-576.7223,-576.7223],ylim,'--k','LineWidth',8)

model_info.Vn_sigma = [2.5,3.125];

samples_nlss_1 = infer('nonlinear_state_space_infer_params',model_info,'smc','n_particles',n_particles,'n_iter',n_iter,'b_parallel',true,'b_compress',true);
log_Zs_1 = samples_nlss_1.other_outputs.log_Zs;

model_info.Vn_sigma = [3.125,3.75];

samples_nlss_2 = infer('nonlinear_state_space_infer_params',model_info,'smc','n_particles',n_particles,'n_iter',n_iter,'b_parallel',true,'b_compress',true);
log_Zs_2 = samples_nlss_2.other_outputs.log_Zs;

figure
bar(xoutall,nall,'b'); hold on;
[n1, xout1] = hist(log_Zs_1,20);
bar(xout1,n1,'r'); grid;
[n2, xout2] = hist(log_Zs_2,20);
bar(xout2,n2,'g');
set(gca,'xlim',xlim);
ylim = get(gca,'ylim');
plot([-576.7223,-576.7223],ylim,'--k','LineWidth',8)

model_info.Vn_sigma = [2.5,2.8125];

samples_nlss_1 = infer('nonlinear_state_space_infer_params',model_info,'smc','n_particles',n_particles,'n_iter',n_iter,'b_parallel',true,'b_compress',true);
log_Zs_1 = samples_nlss_1.other_outputs.log_Zs;

model_info.Vn_sigma = [2.8125,3.125];

samples_nlss_2 = infer('nonlinear_state_space_infer_params',model_info,'smc','n_particles',n_particles,'n_iter',n_iter,'b_parallel',true,'b_compress',true);
log_Zs_2 = samples_nlss_2.other_outputs.log_Zs;

figure
bar(xoutall,nall,'b'); hold on;
[n1, xout1] = hist(log_Zs_1,20);
bar(xout1,n1,'r'); grid;
[n2, xout2] = hist(log_Zs_2,20);
bar(xout2,n2,'g');
set(gca,'xlim',xlim);
ylim = get(gca,'ylim');
plot([-576.7223,-576.7223],ylim,'--k','LineWidth',8)
