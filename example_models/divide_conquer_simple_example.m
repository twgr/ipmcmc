function [sampling_functions,observe_functions] = divide_conquer_simple_example(~)
%% CHANGE THE VALUES ASSIGNED TO THE VARIABLES IN THIS SECTION

n_observes = 1;
observe_funcs_to_call = ones(n_observes,1);
sample_funcs_to_call = ones(n_observes,1);
observe_args = cell(n_observes,1);
sample_args = cell(n_observes,1);


%% LEAVE THIS SECTION AS IS

sampling_functions = cell(n_observes,1);
observe_functions = cell(n_observes,1);

for n=1:n_observes
    sampling_functions{n} = makefunction(true,sample_funcs_to_call(n),sample_args{n,:});
    observe_functions{n} = makefunction(false,observe_funcs_to_call(n),observe_args{n,:});
end

end

%% LEAVE THIS SECTION AS IS

function f = makefunction(bSample,number,varargin)

if bSample
    eval(['f = @(X) sample_' num2str(number) '(X,varargin{:});']);
else
    eval(['f = @(X) observe_' num2str(number) '(X,varargin{:});']);
end    
    
end

function X = sample_0(X,varargin) %#ok<DEFNU>
    % This is a placeholder for doing nothing;
end

%% WRITE THE SAMPLING FUNCTIONS AND OBSERVE FUNCTIONS IN THIS BLOCK

function X = sample_1(X,varargin)

% Sample variable l
X.con.prior = uniform_discrete_class(1,2);
X.var.k = X.con.prior.sample;

% Sample variable r
X = if_function(X,...
    @(X,~) X.var.k==1,...    % Bool for if statement
    @left_branch,...             % Return when true (name and value)
    @right_branch);           % Return when false (function that updates X

end

function log_w = observe_1(X,varargin)

% Observe that poisson(X.var.l) = 6;
lik = gaussian_1d_class(X.var.x,2);
log_w = lik.observe(2);

end


%% Other required functions can be defined here

function [X,empty_output] = left_branch(X,varargin)
empty_output = [];

X.var.x = gaussian_1d_class(-3,1).sample;
end

function [X,empty_output] = right_branch(X,varargin)
empty_output = [];

X.var.x = gaussian_1d_class(3,1).sample;
end